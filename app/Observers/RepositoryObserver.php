<?php

namespace App\Observers;

use App\Models\Package;
use App\Models\Repository;

class RepositoryObserver
{
    /**
     * Handle the repository "created" event.
     *
     * @param  \App\Models\Repository  $repository
     * @return void
     */
    public function created(Repository $repository)
    {
        //
    }

    /**
     * Handle the repository "updated" event.
     *
     * @param  \App\Models\Repository  $repository
     * @return void
     */
    public function updated(Repository $repository)
    {
        //
    }

    /**
     * Handle the repository "deleting" event.
     *
     * @param  \App\Models\Repository  $repository
     * @return void
     */
    public function deleting(Repository $repository)
    {
        $repository->packages->each(function (Package $version) {
            $version->delete();
        });
    }

    /**
     * Handle the repository "deleted" event.
     *
     * @param  \App\Models\Repository  $repository
     * @return void
     */
    public function deleted(Repository $repository)
    {
        if ($repository->avatar) {
            $repository->avatar->delete();
        }
    }

    /**
     * Handle the repository "restored" event.
     *
     * @param  \App\Models\Repository  $repository
     * @return void
     */
    public function restored(Repository $repository)
    {
        //
    }

    /**
     * Handle the repository "force deleted" event.
     *
     * @param  \App\Models\Repository  $repository
     * @return void
     */
    public function forceDeleted(Repository $repository)
    {
        //
    }
}
