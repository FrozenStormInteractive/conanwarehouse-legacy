<?php

namespace App\Conan;

class PackageVersionReference
{
    const PATTERN = '(?P<name>[^/]+)/(?P<version>[^/]+)@(?P<user>[^/]+)/(?P<channel>[^/\#]+)\#?(?P<revision>.+)?';

    // TODO: add revision
    const URL_PATTERN = '[^/]+/[^/]+/[^/]+/[^/\#]+';

    const DIR_PATTERN = '(?P<name>[^/]+)/(?P<version>[^/]+)/(?P<user>[^/]+)/(?P<channel>[^/\#]+)';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $version;

    /**
     * @var string
     */
    protected $user;

    /**
     * @var string
     */
    protected $channel;

    /**
     * @var string
     */
    protected $revision;

    protected static function loadWithPattern($pattern, $str)
    {
        $str = trim($str);

        $matches = [];
        if (preg_match_all("#".$pattern."#", $str, $matches, PREG_SET_ORDER, 0) !== 1) {
            throw new \InvalidArgumentException("Wrong package recipe reference {$str}\nWrite something like ".
                "OpenCV/1.0.6@user/stable");
        }
        return new PackageVersionReference(
            $matches[0]['name'],
            $matches[0]['version'],
            $matches[0]['user'],
            $matches[0]['channel'],
            isset($matches[0]['revision']) ? $matches[0]['revision'] : null
        );
    }

    public static function load($str)
    {
        return self::loadWithPattern(self::PATTERN, $str);
    }

    public static function loadFromUrl($str)
    {
        return self::loadWithPattern(self::DIR_PATTERN, $str);
    }

    protected const MIN_CHARS_IN_NAME = 2;

    protected const MAX_CHARS_IN_NAME = 51;

    protected const VALIDATION_PATTERN = "/^[a-zA-Z0-9_][a-zA-Z0-9_\+\.-]+$/";

    protected const REV_VALIDATION_PATTERN = "/^[a-zA-Z0-9]{1,%d}$/";

    public function __construct(string $name, string $version, string $user, string $channel, string $revision = null)
    {
        $this->name = $this->validate("name", $name);
        $this->version = $this->validate("version", $version);
        $this->user = $this->validate("user", $user);
        $this->channel = $this->validate("channel", $channel);
        if ($revision) {
            $revision = trim($revision);
            $check = preg_match(sprintf(self::REV_VALIDATION_PATTERN, self::MAX_CHARS_IN_NAME), $revision);
            if ($check !== 1) {
                throw new \InvalidArgumentException("The revision field, must contain only letters and ".
                    "numbers with a length between 1 and ".self::MAX_CHARS_IN_NAME);
            }
            $this->revision = trim($revision);
        }
    }

    protected function validate(string $fieldname, string $value) : string
    {
        $value = trim($value);
        if (strlen($value) > self::MAX_CHARS_IN_NAME) {
            throw new \InvalidArgumentException("Value provided for $fieldname, '$value', is too long. Valid ".
                "names must contain at most ".self::MAX_CHARS_IN_NAME." characters.");
        } elseif (strlen($value) < self::MIN_CHARS_IN_NAME) {
            throw new \InvalidArgumentException("Value provided for $fieldname, '$value', is too short. Valid ".
                "names must contain at least ".self::MIN_CHARS_IN_NAME." characters.");
        } elseif (preg_match(self::VALIDATION_PATTERN, $value) !== 1) {
            throw new \InvalidArgumentException("Value provided for name, '$value', is an invalid name. Valid ".
                "names MUST begin with a letter, number or underscore, have between ".self::MIN_CHARS_IN_NAME."-"
                .self::MAX_CHARS_IN_NAME." chars, including letters, numbers, underscore, dot and dash.");
        }
        return $value;
    }

    /**
     * @return string
     */
    public function name() : string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function version() : string
    {
        return $this->version;
    }

    /**
     * @return string
     */
    public function user() : string
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function channel() : string
    {
        return $this->channel;
    }

    /**
     * @return string
     */
    public function revision()
    {
        return $this->revision;
    }

    /**
     * @return string
     */
    public function representation() : string
    {
        $repr = "{$this->name}/{$this->version}@{$this->user}/{$this->channel}";
        if ($this->revision) {
            $repr .= '#'.$this->revision;
        }
        return $repr;
    }

    /**
     * @return string
     */
    public function repr() : string
    {
        return $this->representation();
    }

    public function __toString() : string
    {
        return $this->representation();
    }

    /**
     * @return string
     */
    public function dirRepresentation() : string
    {
        $repr = "{$this->name}/{$this->version}/{$this->user}/{$this->channel}";
        if ($this->revision) {
            $repr .= '/'.$this->revision;
        }
        return $repr;
    }

    /**
     * @return string
     */
    public function dirRepr() : string
    {
        return $this->dirRepresentation();
    }
}
