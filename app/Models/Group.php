<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /*
     * Relationships
     */

    public function avatar()
    {
        return $this->belongsTo(Media::class, 'avatar_id');
    }

    /*
     * Accessors & mutators
     */

    public function getAvatarUrlAttribute()
    {
        if ($this->avatar) {
            return $this->avatar->url;
        } else {
            return asset('images/organization-avatar.png');
        }
    }
}
