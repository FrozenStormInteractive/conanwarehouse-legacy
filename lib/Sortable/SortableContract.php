<?php

namespace Lib\Sortable;

interface SortableContract
{
    public function scopeSort($query);
}
