<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Package::class, function (Faker $faker) {
    return [
        'name' => str_random(20),
        'description' => $faker->text(255),
        'user' => $faker->userName,
        'website_url' => $faker->url,
        'issue_tracker_url' => $faker->url,
        'vcs_url' => $faker->url,
        'visibility' => \App\Enums\VisibilityType::PUBLIC
    ];
});
