<?php
// phpcs:disable Generic.Files.LineLength
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->name('api.search.')->prefix('search')->group(function () {
    Route::get('/users', 'UserController@search')->name('users');
});

Route::middleware('disable-debugbar')->name('api.conan.')->prefix('{repository_id}')->where(['repository_id' => '[0-9]+'])->group(function () {

    $common = function () {
        Route::get('ping', function (int $repository) {
            if (\App\Models\Repository::exists($repository)) {
                return Response::make("", 204);
            }
            abort(404, "Repository not found");
        });

        Route::prefix('users')->group(function () {
            Route::get('/authenticate', 'Api\\Conan\\UserController@authenticate');
            Route::get('/check_credentials', 'Api\\Conan\\UserController@checkCredentials');
        });
    };

    Route::prefix('v1')->group(function () use ($common) {
        $common();

        Route::prefix('conans')->group(function () {
            Route::prefix('/{reference}/')->where(['reference' => \App\Conan\PackageVersionReference::URL_PATTERN])->group(function () {
                Route::get('/', 'Api\\Conan\\V1\\PackageController@snapshot');
                Route::get('/digest', 'Api\\Conan\\V1\\PackageController@digest');
                Route::get('/download_urls', 'Api\\Conan\\V1\\PackageController@downloadUrls');

                Route::post('/upload_urls', 'Api\\Conan\\V1\\PackageController@uploadUrls');

                Route::delete('/', 'Api\\Conan\\V1\\PackageController@destroy');
                Route::post('/remove_files', 'Api\\Conan\\V1\\PackageController@deleteFiles');
                Route::post('/packages/delete', 'Api\\Conan\\V1\\BuildController@delete');

                Route::prefix('/packages/{build_id}')->where(['build_id' => '\w+'])->group(function () {
                    Route::get('/', 'Api\\Conan\\V1\\BuildController@snapshot');
                    Route::get('/digest', 'Api\\Conan\\V1\\BuildController@digest');
                    Route::get('/download_urls', 'Api\\Conan\\V1\\BuildController@downloadUrls');

                    Route::post('/upload_urls', 'Api\\Conan\\V1\\BuildController@uploadUrls');
                });
            });
        });
    });

    Route::prefix('v2')->group(function () use ($common) {
        $common();
    });

    Route::group(['middleware' => 'signed', 'prefix' => '/files/{package_version_id}/', 'where' => ['package_version_id' => '[0-9]+']], function () {
        Route::get('export/{filepath}', 'Api\\Conan\\V1\\PackageController@downloadFile')
            ->where('filepath', '(.*)')
            ->name('package.file_download');

        Route::get('package/{build_id}/{filepath}', 'Api\\Conan\\V1\\BuildController@downloadFile')
            ->where('build_id', '[0-9]+')
            ->where('filepath', '(.*)')
            ->name('build.file_download');

        Route::put('export/{filepath}', 'Api\\Conan\\V1\\PackageController@uploadFile')
            ->where('filepath', '(.*)')
            ->name('package.file_upload');

        Route::put('package/{build_id}/{filepath}', 'Api\\Conan\\V1\\BuildController@uploadFile')
            ->where('build_id', '[0-9]+')
            ->where('filepath', '(.*)')
            ->name('build.file_upload');
    });
});
