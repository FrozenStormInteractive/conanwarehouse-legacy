<div @isset($id) id="{{ $id  }}" @else id="set-me-up-modal" @endisset class="modal fade" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Set Me Up</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="mb-0">Général</h5>
                <div class="mb-3">
                    To add the repository to your conan CLI, use:

                    <div class="code">conan remote add &lt;REMOTE&gt; http://localhost:8081/artifactory/api/conan/conan-local</div>
                    And replace <code>&lt;REMOTE&gt;</code> with a name that identifies the repository (for example: "my-conan-repo")
                    To login use the conan user command:
                    <div class="code">conan user -p &lt;PASSWORD&gt; -r &lt;REMOTE&gt; &lt;USERNAME&gt;</div>
                    And provide your Warehouse username and password or API key.
                    If anonymous access is enabled you do not need to login.
                    For complete conan cli reference see documentation at
                    <a href="https://docs.conan.io" target="_blank">docs.conan.io</a>.
                </div>
                <h5 class="mb-0">Deploy</h5>
                <div class="mb-3">
                    To upload a recipe with its binary packages into a Warehouse repository use the following command:
                    <div class="code">conan upload &lt;RECIPE&gt; -r &lt;REMOTE&gt; --all</div>
                    <code>&lt;RECIPE&gt;</code> is the conan recipe reference you want to upload in the format:
                    <code>&lt;NAME&gt;/&lt;VERSION&gt;@&lt;USER&gt;/&lt;CHANNEL&gt;</code>
                </div>
                <h5 class="mb-0">Resolve</h5>
                <div>
                    To install the dependencies defined in your project's <i>conanfile.txt</i> from an Warehouse conan
                    repo, specify the remote:
                    <div class="code">conan install . -r &lt;REMOTE&gt;</div>
                </div>

            </div>
        </div>
    </div>
</div>
