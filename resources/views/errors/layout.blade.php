<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="/images/conan.png" />

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <style>
            body {
                color: #666;
                text-align: center;
                font-family: "Nunito", Helvetica, Arial, sans-serif;
                margin: auto;
                font-size: 14px;
            }

            h1 {
                font-size: 56px;
                line-height: 100px;
                font-weight: 400;
                color: #456;
            }

            h2 {
                font-size: 24px;
                color: #666;
                line-height: 1.5em;
            }

            h3 {
                color: #456;
                font-size: 20px;
                font-weight: 400;
                line-height: 28px;
            }

            hr {
                max-width: 800px;
                margin: 18px auto;
                border: 0;
                border-top: 1px solid #EEE;
                border-bottom: 1px solid white;
            }

            img {
                max-width: 40vw;
                display: block;
                margin: 40px auto;
            }

            a {
                line-height: 100px;
                font-weight: 400;
                color: #4A8BEE;
                font-size: 18px;
                text-decoration: none;
            }

            .container {
                margin: auto 20px;
            }

            .go-back {
                display: none;
            }

        </style>
    </head>
    <body class="text-center">
        <a href="/">
            <img src="/images/conan.png" alt="Conan Logo" />
        </a>
        <h1>
            {{ $exception->getStatusCode() }}
        </h1>
        <div class="container">
            @yield('content')
            <a href="javascript:history.back()" class="js-go-back go-back">Go back</a>
        </div>
        <script>
            (function () {
                var goBack = document.querySelector('.js-go-back');
                if (history.length > 1) {
                    goBack.style.display = 'inline';
                }
            })();
        </script>
    </body>
</html>
