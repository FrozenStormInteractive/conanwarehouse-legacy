@extends('layouts.main')

@section('main-content')
    <div class="container-fluid">
        <div class="card col-12 p-0 mb-4">
            <div class="card-body ml-1 mr-1">
                <div class="media">
                    <img class="mr-3 avatar" src="{{ $user->avatar_url }}" alt="{{ $user->name }} avatar">
                    <div class="media-body">
                        <h1 class="mt-0"><b>{{ $user->name }}</b></h1>
                        <div>@empty($user->bio) @lang("repos.no-description") @else {{ $user->bio }} @endempty</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card col-12 p-0 mb-4">
            <h3 class="card-header">Activity</h3>
            <div class="card-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquid, autem eos ex laboriosam necessitatibus quia sed sunt vel voluptate. Aspernatur at corporis dolore eius magni quisquam, quod recusandae sapiente?</p>
            </div>
        </div>
        <div class="card col-12 p-0 mb-4">
            <h3 class="card-header">Groups</h3>
            <div class="card-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquid, autem eos ex laboriosam necessitatibus quia sed sunt vel voluptate. Aspernatur at corporis dolore eius magni quisquam, quod recusandae sapiente?</p>
            </div>
        </div>
        <div class="card col-12 p-0 mb-4">
            <h3 class="card-header">Contributed repositories</h3>
            <div class="card-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquid, autem eos ex laboriosam necessitatibus quia sed sunt vel voluptate. Aspernatur at corporis dolore eius magni quisquam, quod recusandae sapiente?</p>
            </div>
        </div>
    </div>
@endsection
