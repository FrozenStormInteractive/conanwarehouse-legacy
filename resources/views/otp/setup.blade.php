@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center auth-panel">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Setup Two-Factor Authentication') }}</div>

                    <div class="card-body">
                        <p>
                            Download the Google Authenticator application from App Store or Google Play Store and scan
                            this code. More information is available in the documentation.
                        </p>
                        <img src="{{ $image }}" alt="Code: {{ $secret }}" class="d-block mx-auto">
                        <div class="alert alert-secondary" role="alert">
                            <h4 class="alert-heading">Can't scan the code?</h4>
                            To add the entry manually, provide the following details to the application on your phone.
                            <ul>
                                <li><strong>Account</strong>: {{ config('app.name') }}:{{ Auth::user()->email }}</li>
                                <li><strong>Key</strong>: {{ $secret }}</li>
                                <li><strong>Time based</strong>: Yes</li>
                            </ul>
                        </div>
                        <form method="POST" action="{{ route('settings.otp.validate') }}">
                            @csrf

                            <div class="form-group">
                                <label for="pin_code" class="text-md-right"><strong>{{ __('Pin code') }}</strong></label>
                                <input id="pin_code" type="text" class="form-control{{ $errors->has('pin_code') ? ' is-invalid' : '' }}" name="pin_code" required autofocus>
                                @if ($errors->has('pin_code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('pin_code') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group mb-0">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Setup two-factor app') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
