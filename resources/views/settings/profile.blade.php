@extends('layouts.settings')

@section('main-content')
    <form class="container-fluid" action="{{ route('settings.profile.update') }}" method="post" enctype="multipart/form-data">
        @method('patch')
        @csrf
        <div class="row">
            <div class="col-lg-4">
                <h4>Avatar</h4>
                <p>
                    You can upload an avatar here.
                </p>

            </div>
            <div class="col-lg-8">
                <avatar-image-input class="mr-3 avatar-xl" name="avatar" src="{{ $user->avatar_url }}"></avatar-image-input>
                @if ($errors->has('avatar'))
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $errors->first('avatar') }}</strong>
                    </span>
                @endif
                <small class="form-text text-muted">
                    The maximum file size allowed is 200KB.
                </small>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-4">
                <h4>Main settings</h4>
                <p>
                    This information will appear on your profile.
                </p>
            </div>
            <div class="col-lg-8">
                <div class="form-row">
                    <div class="col-md-9 form-group">
                        <label for="input-name">Name</label>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="input-name" name="name" placeholder="Name" value="{{ $user->name }}">
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        <small class="form-text text-muted">
                            Enter your name, so people you know can recognize you.
                        </small>
                    </div>
                    <div class="col-md-3 form-group">
                        <label for="input-user-id">User ID</label>
                        <input type="text" class="form-control" id="input-user-id" value="{{ $user->id }}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-email">Email</label>
                    <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="input-email" name="email" value="{{ $user->email }}" placeholder="Email">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    <small class="form-text text-muted">
                        This email will be displayed on your public profile.
                    </small>
                </div>
                <div class="form-group">
                    <label for="input-website">Website</label>
                    <input type="url" class="form-control{{ $errors->has('website_url') ? ' is-invalid' : '' }}" id="input-website" name="website_url" value="{{ $user->website_url }}">
                    @if ($errors->has('website_url'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('website_url') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="input-location">Location</label>
                    <input type="text" class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}" id="input-location" name="location" value="{{ $user->location }}">
                    @if ($errors->has('location'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('location') }}</strong>
                        </span>
                    @endif
                </div>
                <div>
                    <button type="submit" class="btn btn-success">Update profile settings</button>
                </div>

            </div>
        </div>
    </form>
@endsection
