<?php

namespace Tests\Feature\ConanApi;

use App\Models\Repository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ConanApiTest extends TestCase
{
    use RefreshDatabase;

    public function testPing()
    {
        // GIVEN
        $repository = factory(Repository::class)->create();

        // WHEN
        $response = $this->get("/api/{$repository->id}/v1/ping");

        // THEN
        $response->assertStatus(204);
    }

    public function testPingWithNonexistentRepository()
    {
        // GIVEN
        $this->artisan('migrate:fresh');

        // WHEN
        $response = $this->get("/api/1/v1/ping");

        // THEN
        $response->assertNotFound();
    }
}
