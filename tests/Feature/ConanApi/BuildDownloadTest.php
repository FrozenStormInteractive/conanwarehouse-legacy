<?php

namespace Tests\Feature\ConanApi;

use App\Models\Repository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BuildDownloadTest extends TestCase
{
    use RefreshDatabase;

    public function testGetManifest()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $package = factory(\App\Models\Package::class)->create([
            "repository_id" => $repository->id
        ]);
        $version = factory(\App\Models\PackageVersion::class)->create([
            "package_id" => $package->id
        ]);
        $build = factory(\App\Models\PackageBuild::class)->create([
            "package_version_id" => $version->id
        ]);

        // WHEN
        $response = $this->get("/api/{$repository->id}/v1/conans/{$version->reference->dirRepr()}/packages/".
            "{$build->conan_id}/digest");

        // THEN
        $response
            ->assertStatus(200)
            ->assertJsonStructure(['conanmanifest.txt']);
    }

    public function testGetSnapshot()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $package = factory(\App\Models\Package::class)->create([
            "repository_id" => $repository->id
        ]);
        $version = factory(\App\Models\PackageVersion::class)->create([
            "package_id" => $package->id
        ]);
        $build = factory(\App\Models\PackageBuild::class)->create([
            "package_version_id" => $version->id
        ]);

        // WHEN
        $response = $this->get("/api/{$repository->id}/v1/conans/{$version->reference->dirRepr()}/packages/".
            "{$build->conan_id}/");

        // THEN
        $response
            ->assertStatus(200)
            ->assertExactJson([
                'conaninfo.txt' => '63d9cfa1e346717799f0bcd4195f432f',
                'conanmanifest.txt' => 'c0967e26ede6dd090a04ac54952cec15',
                'licenses/LICENSE' => '6e0f14e711836dbaf8aa94dd42850af9',
                'include/args.hxx' => '3a8b36b93cbe80cec10ec7ac987eab6d',
            ]);
    }

    public function testGetDownloadUrls()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $package = factory(\App\Models\Package::class)->create([
            "repository_id" => $repository->id
        ]);
        $version = factory(\App\Models\PackageVersion::class)->create([
            "package_id" => $package->id
        ]);
        $build = factory(\App\Models\PackageBuild::class)->create([
            "package_version_id" => $version->id
        ]);

        // WHEN
        $response = $this->get("/api/{$repository->id}/v1/conans/{$version->reference->dirRepr()}/packages/".
            "{$build->conan_id}/download_urls");

        // THEN
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'conaninfo.txt',
                'conanmanifest.txt',
                'licenses/LICENSE',
                'include/args.hxx',
            ]);
    }

    public function testDownloadFiles()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $package = factory(\App\Models\Package::class)->create([
            "repository_id" => $repository->id
        ]);
        $version = factory(\App\Models\PackageVersion::class)->create([
            "package_id" => $package->id
        ]);
        $build = factory(\App\Models\PackageBuild::class)->create([
            "package_version_id" => $version->id
        ]);

        $response = $this->get("/api/{$repository->id}/v1/conans/{$version->reference->dirRepr()}/packages/".
            "{$build->conan_id}/download_urls");
        $body = $response->json();


        foreach ($body as $filename => $url) {
            // WHEN
            $response = $this->get($url);
            // THEN
            $response->assertOk();
        }
    }
}
