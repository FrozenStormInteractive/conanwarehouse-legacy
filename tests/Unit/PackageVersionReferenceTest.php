<?php

namespace Tests\Unit;

use App\Conan\PackageVersionReference;
use Tests\TestCase;

class PackageVersionReferenceTest extends TestCase
{
    public function testLoad()
    {
        $referenceStr = "opencv/2.4.10 @ lasote/testing";

        $result = PackageVersionReference::load($referenceStr);

        $this->assertSame("opencv", $result->name());
        $this->assertSame("2.4.10", $result->version());
        $this->assertSame("lasote", $result->user());
        $this->assertSame("testing", $result->channel());
        $this->assertNull($result->revision());

        // -------------------------

        $referenceStr = "opencv_lite/2.4.10@phil_lewis/testing";

        $result = PackageVersionReference::load($referenceStr);

        $this->assertSame("opencv_lite", $result->name());
        $this->assertSame("2.4.10", $result->version());
        $this->assertSame("phil_lewis", $result->user());
        $this->assertSame("testing", $result->channel());
        $this->assertNull($result->revision());

        // -------------------------

        $referenceStr = "opencv/2.4.10@3rd-party/testing";

        $result = PackageVersionReference::load($referenceStr);

        $this->assertSame("opencv", $result->name());
        $this->assertSame("2.4.10", $result->version());
        $this->assertSame("3rd-party", $result->user());
        $this->assertSame("testing", $result->channel());
        $this->assertNull($result->revision());

        // -------------------------

        $referenceStr = "opencv/2.4.10@3rd-party/testing#rev1";

        $result = PackageVersionReference::load($referenceStr);

        $this->assertSame("rev1", $result->revision());

        // -------------------------

        $referenceStr = "opencv/2.4.10@lasote/testing#rev1";

        $result = PackageVersionReference::load($referenceStr);

        $this->assertSame("testing", $result->channel());
        $this->assertSame("rev1", $result->revision());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testLoadWithEmptyReferenceString()
    {
        $referenceStr = "";

        PackageVersionReference::load($referenceStr);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testLoadWithBadReferenceString1()
    {
        $referenceStr = "opencv/2.4.10";

        PackageVersionReference::load($referenceStr);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testLoadWithBadReferenceString2()
    {
        $referenceStr = "opencv/2.4.10 @ lasote";

        PackageVersionReference::load($referenceStr);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testLoadWithBadReferenceString3()
    {
        $referenceStr = "opencv??/2.4.10@laso/testing";

        PackageVersionReference::load($referenceStr);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testLoadWithBadReferenceString4()
    {
        $referenceStr = ".opencv/2.4.10@lasote/testing";

        PackageVersionReference::load($referenceStr);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testLoadWithBadReferenceString5()
    {
        $referenceStr = "o/2.4.10 @ lasote/testing";

        PackageVersionReference::load($referenceStr);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testLoadWithBadReferenceString6()
    {
        $referenceStr = "lib/1.0@user&surname/channel";

        PackageVersionReference::load($referenceStr);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testLoadWithBadReferenceString7()
    {
        $referenceStr = "opencv/2.4.10/laso/testing";

        PackageVersionReference::load($referenceStr);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testLoadWithBadReferenceString8()
    {
        $referenceStr = "opencv/2.4.10/laso/test#1";

        PackageVersionReference::load($referenceStr);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testLoadWithBadReferenceString9()
    {
        $referenceStr = "opencv@2.4.10/laso/test";

        PackageVersionReference::load($referenceStr);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testLoadWithBadReferenceString10()
    {
        $referenceStr = "opencv/2.4.10/laso@test";

        PackageVersionReference::load($referenceStr);
    }

    public function testLoadFromUrl()
    {
        // GIVEN
        $referenceStr = "mypackage/1.0.0/conan/stable";

        // WHEN
        $result = PackageVersionReference::loadFromUrl($referenceStr);

        // THEN
        $this->assertSame("mypackage", $result->name());
        $this->assertSame("1.0.0", $result->version());
        $this->assertSame("conan", $result->user());
        $this->assertSame("stable", $result->channel());
    }

    public function testRepresentation()
    {
        // GIVEN
        $reference = new PackageVersionReference("opencv_lite", "2.4.10", "phil_lewis", "testing");

        // WHEN
        $result = $reference->representation();
        $str = (string) $reference;

        // THEN
        $this->assertSame("opencv_lite/2.4.10@phil_lewis/testing", $result);
        $this->assertSame("opencv_lite/2.4.10@phil_lewis/testing", $str);
    }

    public function testRepresentationWithRevision()
    {
        // GIVEN
        $reference = new PackageVersionReference("opencv_lite", "2.4.10", "phil_lewis", "testing", "rev1");

        // WHEN
        $result = $reference->representation();
        $str = (string) $reference;

        // THEN
        $this->assertSame("opencv_lite/2.4.10@phil_lewis/testing#rev1", $result);
        $this->assertSame("opencv_lite/2.4.10@phil_lewis/testing#rev1", $str);
    }

    public function testDirRepresentation()
    {
        // GIVEN
        $reference = new PackageVersionReference("mypackage", "1.0.0", "conan", "stable");

        // WHEN
        $result = $reference->dirRepresentation();

        // THEN
        $this->assertSame("mypackage/1.0.0/conan/stable", $result);
    }
}
